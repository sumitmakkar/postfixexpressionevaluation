#include <iostream>
#include <vector>

using namespace std;

typedef struct node_structure
{
    int    data;
    struct node_structure *next;
}node_struct;

class MyStack
{
    private:
        node_struct *head;
    
        node_struct* createNode(int num)
        {
            node_struct *newNode = new node_struct();
            if(newNode)
            {
                newNode->data = num;
                newNode->next = NULL;
            }
            return newNode;
        }
    
        bool checkIfListIsEmpty()
        {
            return head ? false : true;
        }
    
    public:
        void push(int num)
        {
            node_struct *newNode = createNode(num);
            if(!head)
            {
                head = newNode;
            }
            else
            {
                newNode->next = head;
                head          = newNode;
            }
        }
    
        int pop()
        {
            int num = !checkIfListIsEmpty() ? head->data : -1;
            node_struct *toBeDeletedNode = head;
            head                         = head->next;
            free(toBeDeletedNode);
            toBeDeletedNode              = NULL;
            return num;
        }
};

class Engine
{
    private:
        vector<string> stackList;
        MyStack        myStack;
    
        int convertStringToInteger(string str)
        {
            int len = (int)str.length();
            int num = 0;
            if(!str.compare("+") || !str.compare("*") || !str.compare("-") || !str.compare("/"))
            {
                num = (int)str[0];
            }
            for(int i = 0 ; i < len && str[i] >= '0' && str[i] <= '9' ; i++)
            {
                num = (num * 10) + (str[i] - '0');
            }
            return num;
        }
    
        int performOperation(char op)
        {
            int operand2 = myStack.pop();
            int operand1 = myStack.pop();
            int output   = 0;
            switch (op)
            {
                case '+':
                {
                    output = operand1 + operand2;
                    break;
                }
                case '-':
                {
                    output = operand1 - operand2;
                    break;
                }
                case '/':
                {
                    output = operand1 / operand2;
                    break;
                }
                case '*':
                {
                    output = operand1 * operand2;
                    break;
                }
                    
                default:
                    break;
            }
            return output;
        }
    
    public:
        Engine(vector<string> stList)
        {
            stackList = stList;
            myStack   = MyStack();
        }
    
        int evaluateExpression()
        {
            int len = (int)stackList.size();
            for(int i = 0 ; i < len ; i++)
            {
                int convertedInteger = convertStringToInteger(stackList[i]);
                if(convertedInteger == '+' || convertedInteger == '-' || convertedInteger == '*' || convertedInteger == '/')
                {
                    myStack.push(performOperation((char)convertedInteger));
                }
                else
                {
                    myStack.push(convertedInteger);
                }
            }
            return myStack.pop();
        }
};

int main(int argc, const char * argv[])
{
    vector<string> stackList = {"5" , "1" , "2" , "+" , "4" , "*" , "+" , "3" , "-"};
    //vector<string> stackList = {"20" , "30" , "+"};
    Engine e = Engine(stackList);
    cout<<e.evaluateExpression()<<endl;
    return 0;
}
